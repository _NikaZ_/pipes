﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private List<Sprite> _pipeTypeSprites;

    private PipeData _pipeData;

    public void Init(PipeData pipeData)
    {
        _pipeData = pipeData;
        Rotate(pipeData.Rotation);
        _spriteRenderer.sprite = _pipeTypeSprites[pipeData.PipeType];
        pipeData.OnPipeRotated += Rotate;
    }

    private void OnDestroy()
    {
        _pipeData.OnPipeRotated -= Rotate;
    }

    private void Rotate(int angle)
    {
        Vector3 rotation = transform.eulerAngles;
        rotation.z = angle * 90;
        transform.eulerAngles = rotation;
    }

    private void OnMouseDown()
    {
        _pipeData.Rotate(1);
    }
}