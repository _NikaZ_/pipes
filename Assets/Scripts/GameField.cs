﻿using System.Collections.Generic;
using UnityEngine;

public class GameField : MonoBehaviour
{
    [SerializeField] private PipeView _pipeViewPrefab;
    [SerializeField] private Vector2Int _fieldSize;
    private readonly Dictionary<Vector2Int, PipeData> _pipesData = new Dictionary<Vector2Int, PipeData>();

    private void Start()
    {
        for (int i = 0; i < _fieldSize.y; i++)
        {
            for (int j = 0; j < _fieldSize.x; j++)
            {
                CreatePipe(new Vector2Int(j, i));
            }
        }
    }

    private void CreatePipe(Vector2Int position)
    {
        PipeData pipeData = new PipeData(Random.Range(0, 4), Random.Range(0, 5));
        _pipesData.Add(position, pipeData);
        PipeView pipeView = Instantiate(_pipeViewPrefab);
        pipeView.transform.position = transform.position + (Vector3)(Vector2)position - new Vector3(_fieldSize.x / 2, _fieldSize.y / 2) + (Vector3)Vector2.one/2;
        pipeView.Init(pipeData);
    }
}