using System;
using UnityEngine;

public class PipeData
{
    public event Action<int> OnPipeRotated;
    
    public int Rotation { get; private set; }
    public int PipeType { get; }

    public PipeData(int rotation, int pipeType)
    {
        Rotation = rotation;
        PipeType = pipeType;
    }

    public void Rotate(int i)
    {
        Rotation += 1;
        if (Rotation > 3)
        {
            Rotation = 0;
        }
        OnPipeRotated?.Invoke(Rotation);
    }
}